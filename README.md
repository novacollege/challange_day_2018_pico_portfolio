
[![NovaCollegeLogo](https://www.novacollege.nl/Content/img/logo.png)](https://novacollege.nl)

Nova College Challenge Day
====

# Workshop: Ontwerp jouw digitale visitekaartje


JA! je hebt dè handleiding gevonden voor het maken van jouw digitale visitekaartje!

## Wat is het?

+ In deze workshop ga jij jouw eigen visitekaartje vorm geven met eigen inhoud
+ De inhoud ga jij zelf programmeren met een eenvoudige programmeertaal
+ Het zijn 5 pagina's
    + Welkomspagina (met een foto, titel, subtitel en social media iconen)
    + Wie ben jij in 1 regel tekst (one liner)
    + Skills/Ervaring (met toelichting)
    + 4 foto's die ertoe doen
    + Contact (met een kleine toelichting)
+ Je mag de website mee naar huis nemen

Voor een demonstratie [klik hier!](http://portfoliotest.flashinnovations.nl/)

## Wat heb je nodig?

1. Deze handleiding

## Hoe ga jij dit doen?

1. Visual Studio Code downloaden en installeren (Een programmeerprogramma)
1. Dit project openen met Visual Studio Code
1. Even de bestanden bestuderen die jij kunt aanpassen / gebruiken
1. De website tonen
1. De titel instellen (en testen)
1. De inhoud per pagina instellen samen met de foto's (telkens testen)


### Visual Studio Code downloaden en installeren

1. Download en installeer Visual Studio Code [Klik hier](https://code.visualstudio.com/)
1. Doorloop de installatie
1. Start het programma


### Dit project openen met Visual Studio Code

1. Open het project met Visual Studio Code als volgt:
    1. In het menu: `File => Open Folder`
    1. Navigeer nu naar de locatie van de uitgepakte zipfile
    1. Zodra je `[locatie van uitgepakte zipfile]/Visitekaartje/root` ziet, klik op de `Select Folder` Knop
1. Kijk rustig even rond ( _Geen paniek_ er zijn veel bestanden maar die kun je negeren )

_**De volgende mappen of bestanden zijn belangrijk om bij de hand te houden**_

| Map- of Bestandslocatie  |  Betekenis  | 
|---|---|
| `./config/config.php` | De titel van jouw visitekaartje kan hier aangepast worden |
|  `./assets`  | Hier staan alle plaatjes  |
| `./content/index.md` | In dit bestand programmeer (verander) jij de inhoud van jouw visitekaartje  |

### De website tonen (server starten)

1. Navigeer met de Windows Verkenner naar de locatie waar de zipfile uitgepakt is
1. Navigeer hier naar de map `Visitekaartje`
1. Start het programma `usbwebserver.exe`
1. Open jouw favorite internet browser
1. Type in het adresveld: `http://localhost` [ENTER]
1. Bestudeer jouw visitekaartje (welke je straks aan gaat passen)
1. Klik er eens doorheen / ontdek wat je allemaal kunt doen met het visitekaartje


### De server stoppen

Dit doe je als volgt:

1. Zorg dat Visual Studio Code afgesloten is
1. Zorg dat je USBWebserver op de voorgrond hebt
1. Klik op het tabblad Apache en stop deze
1. Klik op het tabblad Mysql en stop deze ook
1. Klik op het kruisje om USBWebserver af te sluiten

_Je kunt de server ook stoppen door de computer opnieuw op te starten, maar dit is wanneer je de webserver direct wil stoppen_

### Titel van jouw visitekaartje veranderen

1. Zorg dat je weer in Visual Studio Code (VC) bent
1. Open het bestand `./config/config.php`
1. Hier kun je de titel veranderen naar iets wat jij leuk vindt
1. Herlaad de webpagina in jouw browser of jij effect ziet van de verandering

### De teksten per pagina veranderen/instellen

1. Zorg dat je weer in Visual Studio Code (VC) bent
1. Open het bestand `./content/index.md`
1. Bestudeer het eens rustig en probeer de teksten te herkennen
1. Verander eens een tekst en kijk in de browser (herlaad de pagina eerst)

### De foto's aanpassen

1. Je kunt in Windows Verkenner naar de map navigeren waar je de zipfile uitgepakt hebt: `./Visitekaartje/root/assets`
1. Hier staan de foto's (plaatjes) in voor het visitekaartje
1. Als het goed is heb je ze ook gezien in het bestand waar je de teksten aan kunt passen
1. Als jij jouw plaatjes in dit mapje plaatst en ze dezelfde bestandsnaam geeft zullen ze te zien zijn op jouw visitekaartje!
1. Vergeet niet de pagina in de browser te herladen 


# Einde

We hopen dat het gelukt is!


 
====

# Extra informatie

Als je handig bent in webdevelopment dan heb je misschien al in de gaten dat je deze bestanden in z'n geheel op een echte hosting kunt plaatsen.

Dan kan je jouw visitekaartje echt online bekijken (net als: [De demo!](http://portfoliotest.flashinnovations.nl/))

Let dan wel op de vereisten (Zie hieronder de `details voor hackers`).

We wilden echt laten ervaren hoe het is om een webserver te starten, jouw visitekaartje te maken en in de webbrowser te testen.
 

## Details voor Hackers (Andere Developers)
+ Opgezet door: [Ivo Cazemier, Docent Nova College ICT Academie Haarlem](https://github.com/kaasdude) 
+ Het is een CMS systeem ([Pico](http://picocms.org/)) met een thema ([Ethereal](https://github.com/BesrourMS/ethereal))
+ Deze website vereist Apache en PHP 5.3+ en [Composer](https://getcomposer.org/) met voor PHP [dom extension](http://php.net/manual/en/book.dom.php) en [multibyte string](http://php.net/manual/en/book.mbstring.php),  
+ Het CMS gedeelte is voorbereid voor dit specifieke project en daarmee eenvoudige in te vullen
+ Credits:
    + [Pico](http://picocms.org/)
    + [USBWebserver](http://www.usbwebserver.net/webserver/)
    
