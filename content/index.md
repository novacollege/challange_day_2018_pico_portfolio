---

## Pagina 1
Page1Title: Hello My name is Cooper
SubTitle: |
    I'm a spaceman and I am going places
Image: assets/profile-picture.jpg


### Formaat: URL : ICON (Zie voor de icoontjes :https://fontawesome.com/icons?from=io)
Social:
  https://www.linkedin.com/in/rbranson: linkedin
  https://github.com/kaasdude: github
  https://www.facebook.com/Shrek: facebook-f
  https://api.whatsapp.com/send?phone=31613821960: whatsapp
  
  
## Pagina 2
Page2Title: Me in one line
OneLiner: '<em>"Mankind was born on Earth. It was never meant to die here"</em> (Cooper)'
OneLinerImage: assets/skills-picture.jpg


## Pagina 3
Page3Title: Skills
Skills:
  - Test pilot
  - 35 years old
  - Has 2 Kids (Tom and Erin Cooper)
  - Teamplayer
  - Loves to fly around Saturn
SkillsDescription: |
    I love flying in all kinds of aircrafts, especially the ones with troublesome engines.
    
    
## Pagina 4
Page4Title: Experiences
Experiences:
  2017-now : Towing space ships
  2016-2017 : Looking at stuff in harsh conditions
  2010-2016 : Blowing stuff up wherever I go
  2009-2010 : Crashing into the water randomly
### Hier is het mogelijk meer of minder dan 4 foto's te plaatsen (niet uitbreidbaar)
ExperienceImage1: assets/experience-picture-1.jpg
ExperienceImage2: assets/experience-picture-2.jpg
ExperienceImage3: assets/experience-picture-3.jpg
ExperienceImage4: assets/experience-picture-4.jpg


## Pagina 5
Page4Title: Contact
ContactMe: |
    Please contact me!
    I'm stuck behind the book shelf :-S
    This button wil start your email client. It's up to you to contact me, please.
    

### Jouw e-mailadres:
Mailto: github@flashinnovations.nl
MailSubject: Hi, My name is Cooper and I dearly want to be part of the team!
MailMessage: |
     We've always defined ourselves by the ability to overcome the impossible.
     And we count these moments. These moments when we dare to aim higher,
     to break barriers, to reach for the stars, to make the unknown known.
     We count these moments as our proudest achievements. But we lost all that.
     Or perhaps we've just forgotten that we are still pioneers. And we've barely begun.
     And that our greatest accomplishments cannot be behind us, because our destiny lies above us.
     
     Regards Cooper.
     


---
